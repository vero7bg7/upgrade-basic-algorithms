//1.1 Multiplica 10 por 5 y muestra el resultado mediante alert.
const mult = 10 * 5;
console.log(mult);

//1.2 Divide 10 por 2 y muestra el resultado en un alert.
const div1 = 10 / 2;
console.log(div1);

//1.3 Muestra mediante un alert el resto de dividir 15 por 9.
const div2 = 15 / 9;
console.log(div2);

/*1.4 Usa el correcto operador de asignación que resultará en x = 15, 
teniendo dos variables y = 10 y z = 5.*/
const y = 10;
const z = 5;
const x = y + z;
console.log(x);

/*1.5 Usa el correcto operador de asignación que resultará en s = 50,
teniendo dos variables y = 10 y z = 5.*/
const s = y * z;
console.log(s);
